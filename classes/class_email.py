import imghdr
from email.message import EmailMessage
from smtplib import SMTP_SSL
from time import sleep

from validate_email import validate_email


class CEmail:
    __EXTENSIONS = (".pdf", ".txt", ".odt", ".docx", ".xlsx", ".xlsm",
                    ".csv", ".pptx", ".jpg", ".jpeg", ".png", ".gif")

    def __init__(self):
        self.__email_sender = ""
        self.__email_sender_pass = ""
        self.__email_receiver = ""
        self.__subject = ""
        self.__content = ""
        self.__error = ""
        self.__files = []
        self.__images = []
        self.__files_set = False
        self.__images_set = False

    # Getters
    def __get_error(self) -> str:
        return self.__error

    # Setters
    def __set_email_sender(self, email_sender: str) -> None:
        self.__email_sender = email_sender

    def __set_email_sender_pass(self, email_sender_pass: str) -> None:
        self.__email_sender_pass = email_sender_pass

    def __set_email_receiver(self, email_receiver: str) -> None:
        self.__email_receiver = email_receiver

    def __set_subject(self, subject: str) -> None:
        self.__subject = subject

    def __set_content(self, content: str) -> None:
        self.__content = content

    def __set_file(self, files: list[str]) -> None:
        self.__files = files

    def __set_image(self, images: list[str]) -> None:
        self.__images = images

    # Properties
    error = property(fget=__get_error)
    sender = property(fset=__set_email_sender)
    password = property(fset=__set_email_sender_pass)
    receiver = property(fset=__set_email_receiver)
    subject = property(fset=__set_subject)
    content = property(fset=__set_content)
    files = property(fset=__set_file)
    images = property(fset=__set_image)

    # Private methods
    def __validate_extensions(self, paths: list[str], message: str) -> bool:
        checker = False

        for path in paths:
            for extension in self.__EXTENSIONS:
                if path.endswith(extension):
                    checker = True
                    break

                else:
                    checker = False

            if not checker:
                self.__error = message.replace("{}", path)

                return False

        return True

    def __validate_data(self) -> bool:
        if self.__email_sender == "":
            self.__error = "\n[-] Email sender is empty."

            return False

        if not validate_email(self.__email_sender):
            self.__error = "\n[-] Email sender is not valid."
            return False

        if self.__email_sender_pass == "":
            self.__error = "\n[-] Email sender password is empty."
            return False

        if self.__email_receiver == "":
            self.__error = "\n[-] Email receiver is empty."
            return False

        if not validate_email(self.__email_receiver):
            self.__error = "\n[-] Email receiver is not valid."
            return False

        if self.__subject == "":
            self.__error = "\n[-] Email subject is empty."
            return False

        if self.__content == "":
            self.__error = "\n[-] Email content is empty."
            return False

        if self.__files:
            self.__files_set = self.__validate_extensions(
                self.__files, "\n[!] The file '{}' doesn't have a valid extension")

            if not self.__files_set:
                return False

        if self.__images:
            self.__images_set = self.__validate_extensions(
                self.__images, "\n[!] The image '{}' doesn't have a valid extension")

            if not self.__images_set:
                return False

        return True

    # Public methods
    def send_email(self) -> bool:
        message = None

        try:
            if not self.__validate_data():
                return False

            message = EmailMessage()
            message['From'] = self.__email_sender
            message['To'] = self.__email_receiver
            message['Subject'] = self.__subject
            message.set_content(self.__content)

            if self.__files_set:
                for file in self.__files:
                    with open(file, 'rb') as f:
                        file_data = f.read()
                        file_name = f.name.split('/')[-1]

                    message.add_attachment(
                        file_data, maintype='application', subtype='octet-stream', filename=file_name)

            if self.__images_set:
                for img in self.__images:
                    with open(img, 'rb') as f:
                        file_data = f.read()
                        file_name = f.name.split('/')[-1]
                        file_type = imghdr.what(f.name)

                        message.add_attachment(file_data, maintype='image',
                                               subtype=file_type, filename=file_name)

            with SMTP_SSL("smtp.gmail.com", 465) as smtp:
                sleep(1)
                smtp.login(self.__email_sender, self.__email_sender_pass)
                print('\n\n[+] Logged in successfully.')
                print('\n\n[+] Sending email...')
                sleep(1)
                smtp.send_message(message)
                print('\n\n[+] Email sent successfully.')

            return True

        except Exception as ex:
            print("\n\n[!] Error sending email.\n")
            self.__error = str(ex)
            return False

        finally:
            if message != None:
                message = None
