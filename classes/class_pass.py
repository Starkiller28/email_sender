import os
import platform
import random
import string
import subprocess


class CPass:
    # Constructor
    def __init__(self, length: int):
        self.__length = length
        self.__SPECIAL_CHARACTERS = "!@#$%^&*"
        self.__NUMBERS = string.digits
        self.__UPPER_LETTERS = string.ascii_uppercase
        self.__LOWER_LETTERS = string.ascii_lowercase
        self.__CHARACTERS = list(self.__UPPER_LETTERS +
                                 self.__NUMBERS + self.__LOWER_LETTERS + self.__SPECIAL_CHARACTERS)
        self.__password = ""

    # Private methods
    def __generate_password(self) -> str:
        random.shuffle(self.__CHARACTERS)
        passwd = ""

        for _ in range(self.__length):
            passwd += random.choice(self.__CHARACTERS)

        return passwd

    def __validate_counters(self, special_counter: int, numbers_counter: int, upper_counter: int, lower_counter: int) -> bool:
        if special_counter < round(self.__length / 5) or numbers_counter < round(self.__length / 5) or upper_counter < round(self.__length / 5) or lower_counter < round(self.__length / 5):
            return False

        return True

    def __validate_password(self) -> bool:
        special_counter = 0
        numbers_counter = 0
        upper_counter = 0
        lower_counter = 0

        for char in self.__password:
            if char in list(self.__SPECIAL_CHARACTERS):
                special_counter += 1

            elif char in list(self.__NUMBERS):
                numbers_counter += 1

            elif char in list(self.__UPPER_LETTERS):
                upper_counter += 1

            else:
                lower_counter += 1

        if not self.__validate_counters(special_counter, numbers_counter, upper_counter, lower_counter):
            return False

        return True

    def __validate_xclip(self) -> bool:
        exist = subprocess.Popen(["which", "xclip"], stdout=subprocess.PIPE)
        exist = str(exist.communicate()[0])
        exist = exist.replace("b'", "").replace("\\n'", "")

        if exist == "'":
            return False

        return True

    # Public methods
    def get_password(self) -> None:
        print("\n[+] Generating password...")
        self.__password = self.__generate_password()

        while not self.__validate_password():
            self.__password = self.__generate_password()

        print("\n[+] Password generated: ", self.__password)

        if platform.system() == "Linux":
            if self.__validate_xclip():
                self.__password = self.__password.replace('$', '\$')
                os.system(f'echo "{self.__password}" | xclip -sel clip')
                print("\n[+] Password copied to the clipboard")

        elif platform.system() == "Windows":
            os.system(f'echo "{self.__password}" | clip')
            print("\n[+] Password copied to the clipboard")
