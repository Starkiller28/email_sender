
#!/usr/bin/python3.10
# -*- coding: utf-8 -*-


from enquiries import choose

from classes import CEmail, CPass, os, time
from src import validate_is_number, validate_length


def get_file_images_paths(message: str, extensions: str) -> list[str]:
    os.system("clear")
    print(f"\n[+] Enter the full-path of the {message} separated by comma")
    print(f"[!] Allowed extensions {extensions}")
    print("\n[+] Leave empty if you don't want to add any.")
    paths = input("\n[?] ")
    paths = paths.replace(" ", "")
    paths = list(paths.split(","))

    for id, path in enumerate(paths):
        if path == "":
            del paths[id]

    return paths


def send_mail() -> None:
    sender = None
    file_extensions = "(.pdf, .txt, .odt, .docx, .xlsx, .xlsm, .csv, .pptx)"
    image_extensions = "(.jpg, .jpeg, .png, .gif)"
    options = ["Files", "Images"]
    message = "\n  Select (with space bar) if you want to send files, images or both \n  (leave empty if you don't want to send) \n"

    try:
        sender = CEmail()
        sender.sender = "your email"
        sender.password = "your password"
        sender.receiver = input("\n[+] Enter receiver email: ").strip()
        sender.subject = input("\n[+] Enter subject: ").strip()
        sender.content = input("\n[+] Enter content: \n\n").strip()
        answer = choose(message, options, multi=True)

        if len(answer) == 2:
            sender.files = get_file_images_paths("files", file_extensions)
            sender.images = get_file_images_paths("images", image_extensions)

        elif len(answer) == 1:
            if answer[0] == "Files":
                sender.files = get_file_images_paths("files", file_extensions)

            else:
                sender.images = get_file_images_paths(
                    "images", image_extensions)

        if not sender.send_email():
            print(sender.error)

        time.sleep(3)

    except Exception as ex:
        print(f"Type: {type(ex)}")
        print(ex)
        time.sleep(6)

    finally:
        if sender != None:
            sender = None


def generate_pass() -> None:
    message = "\nThe length of the password must be between 15 and 100 characters."
    print(message)
    length = validate_is_number(
        input("\n[+] Enter password length: ").strip(), message)

    length = validate_length(length, message)
    my_pass = CPass(length)

    my_pass.get_password()
    time.sleep(4)

    my_pass = None


def main() -> None:
    try:
        os.system("clear")
        options = ["Send an email", "Generate a password", "Exit"]
        message = '\n  What do you want to do?\n'
        answer = choose(message, options)

        while answer != "Exit":
            os.system("clear")
            if answer == "Send an email":
                send_mail()

            elif answer == "Generate a password":
                generate_pass()

            os.system("clear")
            answer = choose(message, options)

    except KeyboardInterrupt:
        os.system("clear")
        print("\n[!] Exiting...")
        exit(1)

    except Exception as e:
        print(f"type: {type(e)}")
        print(f"Error: {e}")


if __name__ == "__main__":
    main()
